#include <stdio.h>
#include <intrins.h>
#include <string.h>

#include "stc8h.h"
#include "stc32_stc8_usb.h"

void sys_init(void);
void readCommand(void);
void check_buff(u32 addr, u32 chipId);

void lock28();
void unlock28();
void lock29(u32 chipId);
void unlock29(u32 chipId);


char *USER_DEVICEDESC = NULL;
char *USER_PRODUCTDESC = NULL;
char *USER_STCISPCMD = "@STCISP#";

#define PRINTF_HID            //printf输出直接重定向到USB HID接口


//#define MAIN_Fosc        22118400L    //定义主时钟
//#define MAIN_Fosc        12000000L    //定义主时钟
//#define MAIN_Fosc        11059200L    //定义主时钟
//#define MAIN_Fosc         5529600L    //定义主时钟
//#define MAIN_Fosc        24000000L    //定义主时钟
#define MAIN_Fosc        35000000L    //定义主时钟

//========================================================================
// 函数: void delay_ms(unsigned int ms)
// 描述: 延时函数。
// 参数: ms,要延时的ms数, 这里只支持1~255ms. 自动适应主时钟.
// 返回: none.
// 版本: VER1.0
// 日期: 2021-3-9
// 备注: 
//========================================================================
void delay_ms(unsigned int ms)
{
    unsigned int i;
    do{
        i = MAIN_Fosc / 10000;
        while(--i);
    }while(--ms);
}

void delay_us(unsigned int ns)
{
    unsigned int i;
    while(ns)
    {
      i = 10;
      while (i) i--;
      ns--;
    }
}
void delay_50(void)
{
    unsigned int i;
    i = 5;
    while (--i);
}

void delay_100(void)
{
    unsigned int i;
    i = 9;
    while (--i);
}

void delay_500(void)
{
    unsigned int i;
    i = 50;
    while (--i);
}


//缓存
#define BUFF_LEN 256

#define CE P64
#define OE P65
#define WE24 P43
#define WE28 P46
#define WE29 P63
#define VCC24 P45
#define VCC28 P61


// A0-A7   P20-P27
// A8-A15  P40-P47
// A6-A18  P60-P62



u8 cmd;
u8 cmdBuffer[22];
u32 chipId=0,startAddr=0,endAddr=0,dataLength=0;
u8 startPage=0,endPage=0;
u8 char1[1];    // 用于输出
u8 is29 = 0;//是否是29芯片
u16 buff = 64;//最大256

u8 out16[16];

xdata u8 pageBuffer[BUFF_LEN];

const u8  chips_size = 25;
const u32 chips[] = {
  2716,  2732,
  2764,  27128, 27256, 27512,
  27010, 27020, 27040, 27080,
  2816,  2817,
  2863,  2864,  28256,
  29256, 29512,
  29010, 29020, 29040,
  29011, 29021, 29041,
         29022, 29042
};





//支持芯片
const u8  chip29length = 10;
// 290x0 单字节
// 290x1 128字节 包含 AT29512
// 290x2 256字节
const u32 chips29[] = {
    29256,29512,29010,29020,29040,29011,29021,29041,29022,29042
};

// 判断是否是29
void check29InChips(u32 chipId)
{
  u8 i;
  for(i=0;i<chip29length;i++){
    if(chips29[i]==chipId){
      is29 = 1;
      return;
    }
  }
  is29 = 0;
}

void getBufferSize(u32 chipId)
{
  switch(chipId){
      case 29022:
      case 29042:
        buff = 256;
        break;
      case 29512:
      case 29011:
      case 29021:
      case 29041:
      case 29010:
      case 29020:
      case 29040:
        buff = 128;
        break;
      
      case 29256:
        buff = 64;
        break;

      default:
        buff = 64;
        break;
    }
}


// 串口收发 参考 https://www.stcaimcu.com/forum.php?mod=viewthread&tid=2392

void usb_sendchar(char c)
{
  char1[0]=c;
  USB_SendData(char1,1);
  usb_OUT_done();
}    

void usb_send(char cs[],int length)
{
  USB_SendData(cs,length);
  usb_OUT_done();
}


//是否支持
u8 chipSupport(uint32_t chipId){
  int i;
  for(i=0;i<chips_size;i++){
    if(chips[i]==chipId)
      return 1;
  }
  return 0;
}
//检查是否支持并返回结果
void check(uint32_t chipId){
  if(chipSupport(chipId)){
    usb_send("ok\r\n",4);
  }else{
    usb_send("Unsupport\r\n",11);
  }
}




//读取写入数据缓存
void readBuffer()
{
    uint8_t i;
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i]=UsbOutBuffer[i];
          }
          usb_OUT_done();
          return;
        }
      }
    }
}


void readBuffer128()
{
    uint8_t i;
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
         }
      }
    }
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i+64]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
        }
      }
    }
}


void readBuffer256()
{
    uint8_t i;
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
        }
      }
    }
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i+64]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
        }
      }
    }
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i+128]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
        }
      }
    }
    while(1){
      if (bUsbOutReady){
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            pageBuffer[i+192]=UsbOutBuffer[i];
          }
          //usb_OUT_done();
          usb_send("ok\r\n",4);
          break;
        }
      }
    }
}


//执行命令缓存
void readCommand(void)
{
    uint8_t i;
    while(1)
    {
      if (bUsbOutReady)
      {
        if(OutNumber>0){
          for(i=0;i<OutNumber;i++){
            cmdBuffer[i]=UsbOutBuffer[i];
          }
          usb_OUT_done();
         return;
        }
      }
    }
}


u8 hexDigit(char c) {
  if      (c >= '0' && c <= '9') 
    return c - '0';
  else if (c >= 'a' && c <= 'f') 
    return c - 'a' + 10;
  else if (c >= 'A' && c <= 'F') 
    return c - 'A' + 10;
  else 
    return 0;
}

u8 hexByte(char *a) {
  return ((hexDigit(a[0])*16) + hexDigit(a[1]));
}


u32 hexChip(char *dat){
  char* p = dat;
  u32 re = 0;
  if(dat[1]>=0x30 && dat[1]<=0x39)
    re+=(dat[1]-0x30)*10000;
  if(dat[2]>=0x30 && dat[2]<=0x39)
    re+=(dat[2]-0x30)*1000;
  if(dat[3]>=0x30 && dat[3]<=0x39)
    re+=(dat[3]-0x30)*100;
  if(dat[4]>=0x30 && dat[4]<=0x39)
    re+=(dat[4]-0x30)*10;
  if(dat[5]>=0x30 && dat[5]<=0x39)
    re+=(dat[5]-0x30)*1;
  return re;
}

u32 hexWord(char *dat) {
  return (
    (hexDigit(dat[0])*4096)+
    (hexDigit(dat[1])*256)+
    (hexDigit(dat[2])*16)+
    (hexDigit(dat[3])));
}


void read_mode(void){
  // P0 高阻输入
    P0M0 = 0x00; P0M1 = 0xff;
}

void write_mode(void){
  // P0 推挽输出
    P0M0 = 0xff; P0M1 = 0x00;
}

//读数据
u8 read_data_bus(){
    return P0;
}

//写数据
void write_data_bus(char dat){
    P0 = dat;
}


//设置地址
void setAddress(u32 addr,u32 chipId) {

  P2  = addr & 0xff;    
  
  P40 = addr>>8 & 1;
  P41 = addr>>9 & 1;
  P42 = addr>>10 & 1;

  //24P
  if(chipId==2816||chipId==2716){
    VCC24 = 1;//VCC
  }else if(chipId==2732){
    P43 = addr>>11 & 1;
    VCC24 = 1;//VCC
  //28P
  }else if(chipId==2817||chipId==2863||chipId==2864||chipId==28256){
    VCC28 = 1;//VCC
    P43 = addr>>11 & 1;
    P44 = addr>>12 & 1;
    P45 = addr>>13 & 1;
    //跳过WE
    P47 = addr>>14 & 1;
  }else if(chipId==2764||chipId==27128||chipId==27256||chipId==27512){
    VCC28 = 1;//VCC
    P43 = addr>>11 & 1;
    P44 = addr>>12 & 1;
    if(chipId==27512){
      P45 = addr>>13 & 1;
      P46 = addr>>14 & 1;
      P47 = addr>>15 & 1;
    }else if(chipId==27256){
      P45 = addr>>13 & 1;
      P46 = addr>>14 & 1;
    }else if(chipId==27128){
      P45 = addr>>13 & 1;
    }
  }else if(chipId==27010||chipId==27020||chipId==27040||chipId==27080){
//    P43 = addr>>11 & 1;
//    P44 = addr>>12 & 1;
//    P45 = addr>>13 & 1;
//    P46 = addr>>14 & 1;
//    P47 = addr>>15 & 1;
      
    P4  = (addr >> 8) & 0xff;
      
    P60 = addr>>16 & 1;
    if(chipId==27010){
      P62 = 1;//VPP=1
      P61 = 0;
      WE29 = 0;
    }else if(chipId==27020){
      P62 = 1;//VPP=1
      P61 = addr>>17 & 1;
      WE29 = 0;
    }else if(chipId==27040){
      P62 = 1;//VPP=1
      P61 = addr>>17 & 1;
      WE29 = addr>>18 & 1;
    }else if(chipId==27080){
      P61 = addr>>17 & 1;
      WE29 = addr>>18 & 1;
      P62 = addr>>19 & 1;
    }
  //32P
  }else if(is29==1){
//    P43 = addr>>11 & 1;
//    P44 = addr>>12 & 1;
//    P45 = addr>>13 & 1;
//    P46 = addr>>14 & 1;
//    P47 = addr>>15 & 1;
      
    P4  = (addr >> 8) & 0xff;
    
    if(chipId==29010||chipId==29011){
      P60 = addr>>16 & 1;
      P61 = 0;
      P62 = 0;
    }else if(chipId==29020||chipId==29021||chipId==29022){
      P60 = addr>>16 & 1;
      P61 = addr>>17 & 1;
      P62 = 0;
    }else if(chipId==29040||chipId==29041||chipId==29042){
      P60 = addr>>16 & 1;
      P61 = addr>>17 & 1;
      P62 = addr>>18 & 1;
    }
  }
}

// 根据地址读取数据
u8 readByte(u32 addr,u32 chipId){
  setAddress (addr,chipId);

  //延时
  if(chipId==2716||chipId==2732||chipId==2816||chipId==2817||chipId==2863){
    delay_500();
  }else{
    delay_50();
  }

  return read_data_bus();
}

// 读取芯片内容(读取的时候ce oe全部拉低，改变地址就可以输出)
void readBinary(u32 addr, u32 count,u32 chipId) {
  u8 i=0;
  read_mode();
  if(chipId==2816||chipId==2716||chipId==2732){
    VCC24=1;
    WE24=1;
    delay_ms(10);
  }else if(chipId==2817||chipId==2863||chipId==2864||chipId==28256){
    VCC28=1;
    WE28=1;
    delay_ms(10);
  }else if(chipId==2764||chipId==27128||chipId==27256||chipId==27512){
    VCC28=1;
    delay_ms(10);
  }else if(is29==1){
    WE29=1;
  }

  OE=0;
  CE=0;
//  while (count) {
//    usb_sendchar(readByte(addr++, chipId));
//    count--;
//  }
    
  while (count) {
    out16[i]=readByte(addr++, chipId);
    if(i++>=15){
      usb_send(out16,16);
      i=0;
    }
    count--;
  }
  
    
  OE=1;
  CE=1;
}





void write_byte28(u32 address, u8 dat,u32 chipId) {
  write_data_bus(dat);
  setAddress(address,chipId);
  delay_100();
  WE28=0;
  delay_500();
  WE28=1;
  delay_100();
}
void write_byte29(u32 address, u8 dat,u32 chipId) {
  write_data_bus(dat);
  setAddress(address,chipId);
  delay_50();
  WE29=0;
  delay_50();
  WE29=1;
  delay_50();
}


void reset29(u32 chipId){
    OE=1;
    CE=0;
    // 触发复位
    write_byte29 (0x0, 0xf0, chipId);
    CE=1;
    delay_ms(10);
}

void write28(u32 addr,u32 chipId){
  int i=0;
  WE28=1;
    
  // 推挽输出VCC
  P6M0 = 0x02;
  VCC28=1;
    
    
  readBuffer();
  
  OE=1;
  write_mode();
  CE=0;
  delay_ms(1);

  for(i=0;i<64;i++){
    write_byte28(addr+i,pageBuffer[i],chipId);
    if(chipId==2817||chipId==2863){
      delay_ms(1);
    }
  }
    
  if(chipId==2817||chipId==2863){
    delay_ms(10);
  }
  delay_ms(10);

  CE=1;
  read_mode();
  //WE读取
  WE28=1;
  
  delay_500();

  //check_buff(addr,chipId);
  usb_send("ok\r\n",4);

  setAddress(0,chipId);
  
  delay_ms(1);
  
  P6M0 = 0x00;
}

void write29(uint32_t addr,uint32_t chipId){
    u8 i;
    u8 count=100;
    WE29=1;
    
    if(buff==256){
      readBuffer256();
    }else if(buff==128){
      readBuffer128();
    }else{
      readBuffer();
    }
  
  write_mode();
  OE=1;
  CE=0;
  //delay_us(10);
  delay_ms(1);
            
  //
  if(chipId==29010||chipId==29020||chipId==29040){
        // 单字节写入
//        if(addr==0){
//            // 地址0 写入2次解决MX29写入问题
//            for(i=0;i<buff;i++){
//                write_byte29 (0x5555, 0xaa, chipId);
//                write_byte29 (0x2aaa, 0x55, chipId);
//                write_byte29 (0x5555, 0xa0, chipId);
//                write_byte29(addr+i,pageBuffer[i],chipId);
//            }
//        }
      
    
    if(addr==0){
        CE=0;
        // 触发复位
        write_byte29 (0x0, 0xf0, chipId);
        CE=1;
        delay_ms(10);
    }
      
    for(i=0;i<buff;i++){
      CE=0;
      write_byte29 (0x5555, 0xaa, chipId);
      write_byte29 (0x2aaa, 0x55, chipId);
      write_byte29 (0x5555, 0xa0, chipId);
      write_byte29(addr+i,pageBuffer[i],chipId);
      CE=0;
      //delay_us(1);
      delay_500();
    }
  }else{
    write_byte29 (0x5555, 0xaa, chipId);
    write_byte29 (0x2aaa, 0x55, chipId);
    write_byte29 (0x5555, 0xa0, chipId);
    for(i=0;i<buff;i++){
      write_byte29(addr+i,pageBuffer[i],chipId);
    }
    delay_ms(1);
  }


  CE=1;
  read_mode();
  //WE读取
  WE29=1;
  
  if(addr<8192){
    delay_ms(1);
    check_buff(addr,chipId);
  }else{
    usb_send("ok\r\n",4);    
  }
  
  setAddress(0,chipId);
}


void check_buff(u32 addr, u32 chipId){
  u8 i;
  delay_ms(10);
  read_mode();
  if(chipId==2817||chipId==2863||chipId==2864||chipId==28256){
    WE28=1;
    VCC28=1;
    delay_500();
  }
  //读数据校验
  OE=0;
  CE=0;
  delay_500();
    
  for(i=0;i<buff;i++){
    uint8_t rd = readByte(addr+i,chipId);
    if(pageBuffer[i]!=rd){
//      printf("0x");
//      printf("%hhx",addr+i);
//      printf("=");
//      printf("%hhx",rd);
//      printf(",");
//      printf("%hhx",pageBuffer[i]);
//      printf(" ");
//      break;
      OE=1;
      CE=1;
      usb_send("error\r\n",7);
      return;
    }
  }
  OE=1;
  CE=1;
  usb_send("ok\r\n",4);
}


void write2816(uint32_t addr,uint32_t chipId){
  int i=0;
  WE24=1;
  OE=1;
  CE=1;
    
  // 推挽输出VCC
  P4M0 = 0x20;
  VCC24=1;
    
  readBuffer();

  delay_ms(10);
  write_mode();

  for(i=0;i<64;i++){
    write_data_bus(pageBuffer[i]);
    setAddress((addr+i),chipId);
    delay_100();
    WE24=0;
    CE=0;
    //delay_500();
    delay_us(1);
    CE=1;
    WE24=1;
    //delay_us(250);
    //delay_us(250);
    delay_ms(10);
  }

  delay_ms(10);

  CE=1;
  //WE读取
  WE24=1;

  read_mode();

  usb_send("ok\r\n",4);
//  delay_us(50);
//  check_buff(addr,chipId);
//  delay_us(50);

  P4M0 = 0x00;
}

void erase29(chipId){
  uint32_t i=0;
  uint32_t count=0;
  WE29=1;
    
  delay_500();
        
  write_mode();
//  OE=1;
//  CE=0;
//  write_byte29 (0x5555, 0xf0, chipId);
//  CE=1;
  delay_ms(10);
  CE=0;
    
  write_byte29 (0x5555, 0xaa, chipId);             // write code sequence
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x80, chipId);
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x10, chipId);
    
  CE=1;
  delay_ms(10);
  CE=0;
  write_byte29 (0x5555, 0xf0, chipId);
  CE=1;
  usb_send("ok\r\n",4);
}




void lock28(){
  // 推挽输出VCC
  P6M0 = 0x02;
  VCC28=1;
  
  WE28=1;
  OE=1;
  CE=0;
  write_mode();
  write_byte28 (0x5555, 0xaa, 28256);
  write_byte28 (0x2aaa, 0x55, 28256);
  write_byte28 (0x5555, 0xa0, 28256);
  CE=1;
  read_mode();
  //delay_500();
  usb_send("ok\r\n",4);
    
  P6M0 = 0x00;
}


void lock29(uint32_t chipId){
  WE29=1;
  OE=1;
  CE=0;
  write_mode();
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0xa0, chipId);
  CE=1;
  read_mode();
  //delay_500();
  usb_send("ok\r\n",4);
}

void unlock28(){
  // 推挽输出VCC
  P6M0 = 0x02;
  VCC28=1;
  
  WE28=1;
  OE=1;
  CE=0;
  write_mode();
  write_byte28 (0x5555, 0xaa, 28256);
  write_byte28 (0x2aaa, 0x55, 28256);
  write_byte28 (0x5555, 0x80, 28256);
  write_byte28 (0x5555, 0xaa, 28256);
  write_byte28 (0x2aaa, 0x55, 28256);
  write_byte28 (0x5555, 0x20, 28256);
  CE=1;
  WE28=1;
  read_mode();
  //delay_500();
  usb_send("ok\r\n",4);
    
  P6M0 = 0x00;
}

void unlock29(uint32_t chipId){
  WE29=1;
  OE=1;
  CE=0;
  write_mode();
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x80, chipId);
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x20, chipId);
  CE=1;
  WE29=1;
  read_mode();
  //delay_500();
  usb_send("ok\r\n",4);
}


void unlock29NoRes(uint32_t chipId){
  WE29=1;
  OE=1;
  CE=0;
  write_mode();
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x80, chipId);
  write_byte29 (0x5555, 0xaa, chipId);
  write_byte29 (0x2aaa, 0x55, chipId);
  write_byte29 (0x5555, 0x20, chipId);
  CE=1;
  WE29=1;
  read_mode();
  //delay_500();
  //usb_send("ok\r\n",4);
}


void sys_init()
{
    P_SW2 |= 0x80;
    
    P0M0 = 0x00;   P0M1 = 0x00;
    P1M0 = 0x00;   P1M1 = 0x00;
    P2M0 = 0xff;   P2M1 = 0x00;
    P3M0 = 0x00;   P3M1 = 0x00;
    P4M0 = 0xff;   P4M1 = 0x00;
    P5M0 = 0x00;   P5M1 = 0x00;
    P6M0 = 0xff;   P6M1 = 0x00; 
    P7M0 = 0x00;   P7M1 = 0x00;

    P3M0 &= ~0x03;
    P3M1 |= 0x03;
    
    P2 = 0;
    P4 = 0;
    P60 = 0;
    P61 = 0;
    P62 = 0;
    
    WE24=1;
    WE28=1;
    WE29=1;
    
    OE = 1;
    CE = 1;

    IRC48MCR = 0x80;
    while (!(IRC48MCR & 0x01));
    
    USBCLK = 0x00;
    USBCON = 0x90;
}



void main()
{
    sys_init();                                     //管脚初始化
    usb_init();                                     //USB CDC 接口配置
    
    IE2 |= 0x80;                                    //使能USB中断
    EA = 1;
      
    read_mode();
    
    while (DeviceState != DEVSTATE_CONFIGURED);     //等待USB完成配置

    while (1)
    {
            
      readCommand();
            
      cmd = cmdBuffer[0];                  //指令
      chipId    = hexChip(cmdBuffer+2);    //芯片
      startPage = hexDigit(cmdBuffer[9]);
      startAddr = hexWord(cmdBuffer+10);   //开始地址
      endPage   = hexDigit(cmdBuffer[15]);
      endAddr   = hexWord(cmdBuffer+16);   //结束地址
      
      startAddr &= 0xffff;
      endAddr   &= 0xffff;
      startPage &= 0xf;
      endPage   &= 0xf;
      endPage    = endPage < startPage ? startPage : endPage;
      
      
      startAddr = (startPage * 65536) + startAddr;
      endAddr   = (endPage   * 65536) + endAddr;
      
      endAddr = endAddr < startAddr ? startAddr : endAddr;


      // 数据长度
      dataLength = endAddr - startAddr + 1;
            
            
      check29InChips(chipId);
      getBufferSize(chipId);
            
      switch(cmd){
        
        case 'i':
            usb_send("EEPROM_STC\r\n",12);
            break;
        
        case 'v':
            usb_send("1.0\r\n",5);
            break;
        
        case 't':
            usb_send("Ready\r\n",7);
            break;
        
        case 'c':   
            check(chipId);
            break;
                
        case 'l':
            if(is29==1){
              lock29(chipId);
            }else{
              lock28();
            }
            break;
        
        case 'u':
            if(is29==1){
              unlock29(chipId);
            }else{
              unlock28();
            }
            break;
        
        // 擦除
        case 'e':
            if(is29==1){
              erase29(chipId);
            }
            break;
                        
        case 'r':
            readBinary(startAddr, dataLength,chipId);
            break;

        case 'w':
            if(chipId==2764||chipId==27128||chipId==27256||chipId==27512){
              //write27(startAddr,dataLength,chipId);
              break;
            }else if(chipId==2817||chipId==2863||chipId==2864||chipId==28256){
              write28(startAddr,chipId);//长度使用buff
              break;
            }else if(chipId==2816){
              write2816(startAddr,chipId);//长度使用buff
              break;
            }else if(is29==1){
              if(startAddr == 0){
                unlock29NoRes(chipId);
              }
              write29(startAddr,chipId);
              break;
            }else{
              usb_send("Write unsupport\r\n",17);
              break;
            }


        // 测试代码
        case 'a':
            read_mode();
            //setAddress(startAddr, chipId);
            usb_sendchar(readByte(startAddr, chipId));
            delay_ms(250);
            delay_ms(250);
            break;
        case 'b':
            write_mode();
            setAddress(startAddr, chipId);
            write_data_bus(0xff);
            delay_ms(250);
            delay_ms(250);
            break;
        case 'd':
            write_mode();
            write_data_bus(0x00);
            delay_ms(250);
            delay_ms(250);
            break;
            case 'p':
            read_mode();
            usb_send("Test tx rx\r\n",12);
            break;

        default:    break;
        
      }
        
      read_mode();
      setAddress(0, 29010);
            
      WE24=1;
      WE28=1;
      WE29=1;
            
      OE = 1;
      CE = 1;
            
    }
}


