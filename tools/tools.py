import struct

file010 = "010-0F.bin"
file020 = "020-0F.bin"
file040 = "040-0F.bin"


with open(file010,mode='wb+') as f:
    for i in range(2**17):
        # if i&0xff==0:
        #     s = struct.pack('B',(i>>8)&0xff)
        #     f.write(s)
        # else:
            s = struct.pack('B',i&0xff)
            f.write(s)

with open(file020,mode='wb+') as f:
    for i in range(2**18):
        # if i&0xff==0:
        #     s = struct.pack('B',(i>>8)&0xff)
        #     f.write(s)
        # else:
            s = struct.pack('B',i&0xff)
            f.write(s)

## 测试芯片用
with open(file040,mode='wb+') as f:
    for i in range(2**19):
        # if i&0xff==0:
        #     s = struct.pack('B',(i>>8)&0xff)
        #     f.write(s)
        # else:
            s = struct.pack('B',i&0xff)
            f.write(s)